# dr-sender-service

Service to send Digital Receipts from the Scanner Service or other compliant service to the customer email address

## Design notes

1. Any email address that fails validation (syntax, external check service, mailgun etc) will have this recorded in ``z_mcore_digital_receipt_reports`` (new column)
1. Resend will not attempt to send to an known invalid email address, unless forced to (flag)


## Send From Scanner Service Request

![alt text](diagrams/DR%20Sender%20Service.png "Basic Send DR Process")


## Resend Request

It is possible to request that a DR is resent. To do this, a call is made supplying the Transaction ID, which is used to retrieve the Promotion ID
and associated data from the OPS D/B (Decision Status).


## Endpoints

### Send (/drsender/v1/send/<transaction ID>/email/<address>)

### Send (/drsender/v1/send/<transaction ID>/loyaltyid/<loyalty id>)

1. Transaction ID (UUID) - Mandatory
1. Loyalty ID (String)
1. EMail address (String)


### Resend (/drsender/v1/resend/<transaction ID>)

### Resend (/drsender/v1/resend/<transaction ID>/force)

1. Transaction ID (UUID) - Mandatory
1. The force flag must take the value ```force``` or ```auto``` to control resending email address checking


### Validate (GET /drsender/v1/validate)
1. Content-Type must be application/xml
1. Body should contain a payload similar to the following xml
```
<ecrebo>
    <request action="user_info">
        <email>[email here]</email>
        <print>true</print>
        <action>send</action>
        <display_time_ms>10588</display_time_ms>
        <extra_info>
            <author>cashier</author>
            <num_input_events>10</num_input_events>
            <num_suggestions_cleared>0</num_suggestions_cleared>
        </extra_info>
    </request>
</ecrebo>
```
